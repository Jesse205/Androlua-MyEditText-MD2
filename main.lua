require "import"
--require "Pretend"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"
import "MyEditText"
activity.setTitle('程序标题')

activity.setContentView(loadlayout("layout"))

print(test1:getText())
print(test1:getHint())

test1:setHint("TEST1")
--testEdit:setText("Text")
--testEdit:setFocusable(true)
--testEdit:setFocusableInTouchMode(true)

--没有api的可以获取控件ID然后自己写
print(test1.EditText)--编辑框的

print(test1.HintView)--Hint的
print(test1.MainView)--主view，可以用getChildAt往下套
test1.onTextChanged=function(...)
  print(...)
end
--test1.EditText.setError("test")
--error先用这个吧
