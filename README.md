# Androlua MyEditText MD2

#### 介绍
使用 Androlua 编写的高仿 Material Design 2的 EditText


#### 下载
1.  到 [统计-发行版](Jesse205/Androlua-MyEditText-MD2/releases) 处下载源码
2.  选择文件-导入源码

#### 使用说明
参见 `layout.aly` 、`main.lua` 、`MyEditText.lua`
注：因技术问题，只能使用id.getText()、id.setText()，不能直接使用id.text
